## About me

I'm Becca Mizzi-Harris. An Interior Designer with 9+ years commercial experience in interior design, fit-out, refurbishment and FF&E.

My sweet spot - Workplace and Office Design. But I love all things Commercial Interiors.

I know too well the stresses of the job, and not having enough hours in the day during busy periods. Squeezing the hours into evenings and weekends isn't healthy or sustainable.

I've been on a mission the last few years, to help interior designers and teams reduce heavy workloads, and allow you to grow your client base and design power sustainably and with ease.

I work with amazing interior designers and teams across the UK, transforming and delivering transformation of commercial spaces that benefit and improve the environments of people, businesses and brands.

Available for freelance and short–mid term contracts, tailored for you.

I'm an email away, becca@mizziharris.com.

Would love to hear from you 😊

### Qualifications

BA (Hons) Interior Architecture

### Software

Revit, AutoCAD, Vectorworks, Enscape, Illustrator, Photoshop, InDesign
