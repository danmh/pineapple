require 'open-uri'
require 'rexml/document'

module Jekyll
  class PodcastFeed < Generator
    def generate(site)
      puts "PodcastFeed generator running"
      feed_url = "https://anchor.fm/s/f89ebc10/podcast/rss"
      begin
        feed_content = URI.open(feed_url).read
        puts "Feed fetched successfully, content length: #{feed_content.length}"
        
        doc = REXML::Document.new(feed_content)
        puts "Feed parsed successfully"
        
        channel = REXML::XPath.first(doc, "//channel")
        site.data['podcast'] = {
          'name' => REXML::XPath.first(channel, "./title").text,
          'description' => REXML::XPath.first(channel, "./description").text,
          'link' => "https://podcasters.spotify.com/pod/show/designandbuild"
        }
        
        site.data['podcast_episodes'] = REXML::XPath.match(doc, "//item").map do |item|
          link = REXML::XPath.first(item, "./link").text
          episode_id = link.split('/').last
          { 
            'title' => REXML::XPath.first(item, "./title").text,
            'description' => REXML::XPath.first(item, "./description").text,
            'pubDate' => REXML::XPath.first(item, "./pubDate").text,
            'url' => REXML::XPath.first(item, "./enclosure/@url").value,
            'duration' => REXML::XPath.first(item, "./itunes:duration").text,
            'episode_type' => REXML::XPath.first(item, "./itunes:episodeType").text,
            'season' => REXML::XPath.first(item, "./itunes:season").text,
            'spotify_id' => episode_id,
            'spotify_link' => link
          }
        end
        puts "Processed #{site.data['podcast_episodes'].length} episodes"
      rescue => e
        puts "Error in PodcastFeed generator: #{e.message}"
        site.data['podcast_episodes'] = []
      end
    end
  end
end