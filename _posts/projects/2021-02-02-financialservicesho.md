---
layout: project
permalink: /:title/
category: projects

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'includes' section for now
  keywords: :includes

project:
  title: "Financial Services"
  includes: "Office"
# thumbnail should be square 800 x 800 px. Put it in the project folder and name it thumbnail.jpg
  thumbnail: "thumbnail.jpg"
  responsibilities: "Interior Design, Space Planning, Client & Supplier Liaison, Furniture Specification, Detailed Drawings, Presentations, Quotations, Scheduling"
  tools: "AutoCad, Adobe Illustrator, Adobe InDesign, Microsoft Office"

collaborators:
  names: "COS Interiors"

# Name of the folder including the trailing /. The bit after 'assets/images/'
imagelocation: "projects/financialservicesho/"

images:
  - image:
    filename: "1.jpg"
    alt: ""
  - image:
    filename: "2.jpg"
    alt: ""
  - image:
    filename: "3.jpg"
    alt: ""
  - image:
    filename: "4.jpg"
    alt: ""
  - image:
    filename: "5.jpg"
    alt: ""
  - image:
    filename: "6.jpg"
    alt: ""
  - image:
    filename: "7.jpg"
    alt: ""
  - image:
    filename: "8.jpg"
    alt: ""
  - image:
    filename: "9.jpg"
    alt: ""
  - image:
    filename: "10.jpg"
    alt: ""
  - image:
    filename: "11.jpg"
    alt: ""
  - image:
    filename: "12.jpg"
    alt: ""
  - image:
    filename: "13.jpg"
    alt: ""
  - image:
    filename: "14.jpg"
    alt: ""
  - image:
    filename: "15.jpg"
    alt: ""
  - image:
    filename: "16.jpg"
    alt: ""
  - image:
    filename: "17.jpg"
    alt: ""
  - image:
    filename: "18.jpg"
    alt: ""
  - image:
    filename: "19.jpg"
    alt: ""
  - image:
    filename: "20.jpg"
    alt: ""
---
Launch and rebrand of Head Office. I designed, developed and organised delivery of new furniture to support the new brand. I created bespoke items, and made fabric and finish selections to be extended to all [branches](/financialservices) and Head Office. I worked closely with the client and third party contractors to ensure the results stayed on brand and delivered a consistent experience for staff and customers.