---
layout: project
permalink: /:title/
category: projects

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'includes' section for now
  keywords: :includes

project:
  title: "Financial Services"
  includes: "Retail"
# thumbnail should be square 800 x 800 px. Put it in the project folder and name it thumbnail.jpg
  thumbnail: "thumbnail.jpg"
  responsibilities: "Interior Design, Space Planning, Client & Supplier Liaison, Furniture Specification, Detailed Drawings, Presentations, Project Management, Quotations, Graphic Design, Scheduling"
  tools: "AutoCad, Adobe Illustrator, Adobe InDesign, Microsoft Office"

collaborators:
  names: "COS Interiors"

# Name of the folder including the trailing /. The bit after 'assets/images/'
imagelocation: "projects/financialservices/"

images:
  - image:
    filename: "1.jpg"
    alt: ""
  - image:
    filename: "3.jpg"
    alt: ""
  - image:
    filename: "4.jpg"
    alt: ""
  - image:
    filename: "5.jpg"
    alt: ""
  - image:
    filename: "6.jpg"
    alt: ""
  - image:
    filename: "7.jpg"
    alt: ""
  - image:
    filename: "8.jpg"
    alt: ""
  - image:
    filename: "9.jpg"
    alt: ""
  - image:
    filename: "10.jpg"
    alt: ""
  - image:
    filename: "11.jpg"
    alt: ""
  - image:
    filename: "12.jpg"
    alt: ""
  - image:
    filename: "13.jpg"
    alt: ""
  - image:
    filename: "14.jpg"
    alt: ""
  - image:
    filename: "2.jpg"
    alt: ""
---
Launch and rebrand of concept retail branch. I designed, developed and organised delivery of new furniture to support the new brand. I created bespoke items, and made fabric and finish selections to be extended to all branches and [Head Office](/financialservicesho). I worked closely with the client and third party contractors to ensure the results stayed on brand and delivered a consistent experience for customers. Currently rolling out to 50+ branches across Wales.