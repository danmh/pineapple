---
layout: project
permalink: /:title/
category: projects

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'includes' section for now
  keywords: :includes

project:
  title: "Property Development"
  includes: "Residential & Office"
# thumbnail should be square 800 x 800 px. Put it in the project folder and name it thumbnail.jpg
  thumbnail: "thumbnail.jpg"
  responsibilities: "Interior Design, Space Planning, Conceptual Ideas, Client & Supplier Liaison, Furniture & Décor Specification, Site Surveys & Quotations"
  tools: "AutoCad, Revit, Adobe Illustrator, Promarker"

collaborators:
  names: "Property Developers"

# Name of the project folder including the trailing /. The bit after 'assets/images/'
imagelocation: "projects/propertydevelopment/"

images:
  - image:
    filename: "8.jpg"
    alt: ""
  - image:
    filename: "9.jpg"
    alt: ""
  - image:
    filename: "10.jpg"
    alt: ""
  - image:
    filename: "11.jpg"
    alt: ""
  - image:
    filename: "12.jpg"
    alt: ""
  - image:
    filename: "13.jpg"
    alt: ""
  - image:
    filename: "14.jpg"
    alt: ""
  - image:
    filename: "15.jpg"
    alt: ""
  - image:
    filename: "16.jpg"
    alt: ""
  - image:
    filename: "17.jpg"
    alt: ""
  - image:
    filename: "18.jpg"
    alt: ""
  - image:
    filename: "19.jpg"
    alt: ""
  - image:
    filename: "20.jpg"
    alt: ""
  - image:
    filename: "21.jpg"
    alt: ""
  - image:
    filename: "26.jpg"
    alt: ""
  - image:
    filename: "1.jpg"
    alt: ""
  - image:
    filename: "2.jpg"
    alt: ""
  - image:
    filename: "3.jpg"
    alt: ""
  - image:
    filename: "4.jpg"
    alt: ""
  - image:
    filename: "5.jpg"
    alt: ""
  - image:
    filename: "6.jpg"
    alt: ""
  - image:
    filename: "7-1.jpg"
    alt: ""
  - image:
    filename: "22.jpg"
    alt: ""
  - image:
    filename: "23.jpg"
    alt: ""
  - image:
    filename: "24.jpg"
    alt: ""
  - image:
    filename: "24.jpg"
    alt: ""
  - image:
    filename: "25.jpg"
    alt: ""
  - image:
    filename: "27.jpg"
    alt: ""
---
Designing contemporary accomodation for short term rentals and commercial office space. Creating solutions that maximise space usage, and curating furniture and finishes aligned to budgets and lease goals. End to end service for property developers from pre-purchase to rental. 
