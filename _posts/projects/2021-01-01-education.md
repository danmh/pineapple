---
layout: project
permalink: /:title/
category: projects

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'includes' section for now
  keywords: :includes

project:
  title: "UWC Atlantic College"
  includes: "Education"
# thumbnail should be square 800 x 800 px. Put it in the project folder and name it thumbnail.jpg
  thumbnail: "thumbnail.jpg"
  responsibilities: "Interior Design, Space Planning, Conceptual Ideas, Client & Supplier Liaison, Furniture & Decor Specification, Detailed Drawings, Mood Boards, Presentations, Quotations"
  tools: "AutoCad, Adobe Illustrator, Adobe InDesign, Microsoft Office"

collaborators:
  names: "Office Essentials, UWC Atlantic College"

# Name of the folder including the trailing /. The bit after 'assets/images/'
imagelocation: "projects/education/"

images:
  - image:
    filename: "1.jpg"
    alt: ""
  - image:
    filename: "2.jpg"
    alt: ""
  - image:
    filename: "3.jpg"
    alt: ""
  - image:
    filename: "4.jpg"
    alt: ""
  - image:
    filename: "5.jpg"
    alt: ""
  - image:  
    filename: "6.jpg"
    alt: ""
  - image:
    filename: "7.jpg"
    alt: ""
  - image:
    filename: "8.jpg"
    alt: ""
  - image:
    filename: "9.jpg"
    alt: ""
  - image:
    filename: "10.jpg"
    alt: ""
  - image:
    filename: "11.jpg"
    alt: ""
  - image:
    filename: "12.jpg"
    alt: ""
  - image:
    filename: "13.jpg"
    alt: ""
  - image:
    filename: "14.jpg"
    alt: ""
  - image:
    filename: "15.jpg"
    alt: ""
  - image:
    filename: "16.jpg"
    alt: ""
---
Working with Office Essentials and their client UWC Atlantic college. I designed and specified a welcoming learning space filled with natural light, featuring the UWC Atlatic vibrant brand colour palette. Created multi-use spaces with brand new collaborative furniture, enabling flexibility in teaching and learning. Incorporating spaces for focussed work, student collaboration and larger group teaching. New power/data, flooring, decor and various building works to get the space functioning perfectly for the student and staff on site.