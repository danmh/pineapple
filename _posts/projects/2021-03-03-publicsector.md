---
layout: project
permalink: /:title/
category: projects

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'includes' section for now
  keywords: :includes

project:
  title: "Public Sector"
  includes: "Office & Cafe"
# thumbnail should be square 800 x 800 px. Put it in the project folder and name it thumbnail.jpg
  thumbnail: "thumbnail.jpg"
  responsibilities: "Interior Design, Space Planning, Conceptual Ideas, Client & Supplier Liaison, Furniture & Decor Specification, Detailed Drawings, Mood Boards, Presentations, Quotations, Graphic Design"
  tools: "AutoCad, Adobe Illustrator, Adobe InDesign, Microsoft Office"

collaborators:
  names: "Powell, 4Real, Third Party Contractors"

# Name of the folder including the trailing /. The bit after 'assets/images/'
imagelocation: "projects/publicsector/"

images:
  - image:
    filename: "4.jpg"
    alt: ""
  - image:
    filename: "5.jpg"
    alt: ""
  - image:
    filename: "6.jpg"
    alt: ""
  - image:
    filename: "7.jpg"
    alt: ""
  - image:
    filename: "8.jpg"
    alt: ""
  - image:  
    filename: "1.jpg"
    alt: ""
  - image:
    filename: "2.jpg"
    alt: ""
  - image:
    filename: "3.jpg"
    alt: ""
  - image:
    filename: "9.jpg"
    alt: ""
  - image:
    filename: "10.jpg"
    alt: ""
  - image:
    filename: "11.jpg"
    alt: ""
  - image:
    filename: "12.jpg"
    alt: ""
  - image:
    filename: "13.jpg"
    alt: ""
  - image:
    filename: "14.jpg"
    alt: ""
  - image:
    filename: "15.jpg"
    alt: ""
  - image:
    filename: "16.jpg"
    alt: ""
  - image:
    filename: "17.jpg"
    alt: ""
  - image:
    filename: "18.jpg"
    alt: ""
  - image:
    filename: "19.jpg"
    alt: ""
  - image:
    filename: "20.jpg"
    alt: ""
  - image:
    filename: "21.jpg"
    alt: ""
  - image:
    filename: "22.jpg"
    alt: ""
  - image:
    filename: "23.jpg"
    alt: ""
  - image:
    filename: "24.jpg"
    alt: ""
  - image:
    filename: "25.jpg"
    alt: ""
  - image:
    filename: "26.jpg"
    alt: ""
  - image:
    filename: "27.jpg"
    alt: ""
  - image:
    filename: "28.jpg"
    alt: ""
  - image:
    filename: "29.jpg"
    alt: ""
---
I designed large scale workplaces across the clients portfolio of buildings, delivering changes that improved the workplace experiences of their staff all over Wales. Responsible for designing solutions that encouraged productivity, enabling flexible working, maximising space and specifying sustainable products.
