---
layout: project
permalink: /:title/
category: projects

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'includes' section for now
  keywords: :includes

project:
  title: "Automotive & Manufacturing"
  includes: "Office"
# thumbnail should be square 800 x 800 px. Put it in the project folder and name it thumbnail.jpg
  thumbnail: "thumbnail.jpg"
  responsibilities: "Interior Design, Space Planning, Client & Supplier Liaison, Furniture and Decor Specification, Detailed Drawings, Presentations, Quotations, Site Surveying, 3D Visualization"
  tools: "Revit, Adobe InDesign, Microsoft Office"

collaborators:
  names: "COS Interiors"

# Name of the folder including the trailing /. The bit after 'assets/images/'
imagelocation: "projects/automotive/"

images:
  - image:
    filename: "1.jpg"
    alt: ""
  - image:
    filename: "2.jpg"
    alt: ""
  - image:
    filename: "3.jpg"
    alt: ""
  - image:
    filename: "4.jpg"
    alt: ""
  - image:
    filename: "6.jpg"
    alt: ""
  - image:
    filename: "7.jpg"
    alt: ""
  - image:
    filename: "9.jpg"
    alt: ""
---
Revamping the entrance and meeting spaces of automotive parts manufacturing factory. Extending the brand into physical spaces. Creating a welcoming area for visitors and employees while continuing to control access to the factory floor.
