---
layout: blog
category: blogs
permalink: /7-step-interior-design-process/

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'summary' section for now
  keywords: :summary

blog:
# This is the title that displays on the page
  title: "My 7 step interior design process"
# Author and visible date appear in the format 'Written by {author}, {visible-date}.
  author: "Becca"
  visible-date: "Mar 2023, 6 min read"
# This is your short extract, it will show on shares and search engines, make it good.
  summary: "Learn about my 7-step interior design process developed over the last 7 years in my commercial interior design career. From the design brief to presentation. Check out my flexible design approach in this blog post."

---

<!-- Add your content using markdown formatting below.                                   -->
<!-- See https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet                -->
<!-- Add images to /assets/images/blog/postname . Reference images using cloudinary.     -->
<!-- Example: https://mizziharris.mo.cloudinary.net/portfolio/blog/examplepost/image.jpg -->

Over the last 7 years of my commercial interior design career so far, I have been refining and fine-tuning my process when dealing with client projects. From start to completion!

Now that I have pivoted into freelance interior design for interior design businesses and property developers. My design process varies around the following 7 steps below.

Sometimes I follow this process in this order, sometimes I am only needed to provide a few of these steps. It can vary from project to project! Depending on what my freelance clients want help with.

I am a firm believer that there are lots of right ways to do things. Not one! So, if your process varies to this - know that this isn’t an exhaustive list or a list of how it ‘should’ be done.

The variables change, designer to designer, client to client, and project to project. This is what I have developed over the years in my commercial interior design career in South Wales, UK.

Hope you find this an interesting, helpful read! Let’s go…

### 1. The Design Brief

The critical first step (and always the first step!)

A clear and thorough brief helps an interior designer understand the client's needs, preferences, and expectations. It sets the foundation for the entire project.

Some designers like to send questionnaires before a meeting or instead of a meeting. if it’s short, and not too labour or time intensive for the client - a mini questionnaire is a good idea to get a basic idea of the enquiry and prepares the clients for what to think about.

But, I think it is important to speak to the client at this stage, whether that be in person or via a video call. I want to make sure we are on the same page. Which can prevent misunderstandings and miscommunications down the line.

I’m all about building relationships and helping people, I want to save my clients time, money and stress! So having an easy back-and-forth relationship makes this easier, for me and the client. I aim to feel part of the client’s team, not an outsider. An email trail isn't great for nurturing relationships (in my opinion!)

My site meeting may be at the same time as the design brief meeting. I measure up the interior space, and take LOTS of photos when attending the site!

*Tip: You never know when you need to see a particular area when drafting and trying to understand the space. Getting this information from the get-go will avoid hassling clients for more information. Important when you’re trying to get cracking on the project!*

Often, clients have plans already. If a client sends a plan, then I will usually request photos, and a ceiling height, if not stated.

### 2. Design and Concept

Once the brief is clear and concise, I take this information and plan out what needs to be done, in priority order. What gets the client what they need to see, to progress to the next stage?

I want to reduce the time spent making revisions and get the brief nailed as soon as possible!

I try to avoid doing steps 1-7 stated here, all at once. In my 7+ years of experience, I have found this results in significant time spent making revisions to each of these stages. Possibly many times!

Clients can change their minds once they see it laid out in front of them; you know, those times when the brief starts to change! So doing this in bite-size steps makes this easier to manage.

I usually start with a mood board. Ideas I have around furniture, colour scheme, floor and wall finishes, using brand assets (if applicable) to get a feel for the space.

Accompanied by a drawn-up site survey and space plan with a proposed layout.

This gives my interior design clients food for thought, and enough to make decisions to proceed with steps 3 & 4!

### 3. Interior Design Drawings

Once all the above is agreed upon, I start drawing up in more detail. With all finishes and user journeys within the space in mind. This drawing step and the next step are usually done side by side for me, project dependant.

Types of drawings I’d be working on (not always all needed):

- Furniture layouts
- Wall finishes
- Floor finishes
- Sections and elevations (needed to visually show any design decisions made on walls, furniture, partitions, glass manifestation, signage etc)
- Partitions plan
- Lighting and ceiling plans
- Door and window schedules
- Electrical plans
- Demolitions plan (showing before and after - for refurbishment and construction projects only)
- Detail drawings (more on this in step 6)

#### Software

I would usually use Revit for all this, a great drafting 3D BIM software. As you draw in 2D it builds in 3D - saves me so much time when having to show elevations, sections and 3D visualisation. Reduces the need to draw and create these aspects individually, and the changes that may come with future revisions. Also reduces human error!

Another software I use is AutoCAD - for small projects with less need for 3D visualisation. (Plenty of software options out there - I used to use SketchUp a lot too).

The above drawings, created ready to hand over to a project manager and for construction/site. Once all the revisions are complete and the project is finalised!

### 4. Specification and FF&E

Alongside step 3, I would start contacting suppliers or scouring the web for furniture, lighting, wall finishes and floor finishes.

A process I love (although I know some hate it) is getting my furniture and specification spreadsheet out and starting to populate my chosen items.

Info I would list on my spec sheet would be:

- What the product is (chair, sofa, desk etc)
- Where the product is from - supplier or weblink
- Specific product code
- Fabric or colour finishes - detail on 2-tone colour, specific supplier fabric choice and code, wood or leg colour, edging, and all the details that come with FF&E (furniture, fixtures and equipment)
- Indicative example image (as close to what I have chosen as I can find)
- Notes and comments - this is good for any extra info needed. E.g. bespoke items with specific instructions and product care or features which are important to the client
- Quantity needed for each item
- Client price per item, and for total quantity needed
- Grand total

I sometimes break this specification down into sections, if there are multiple storeys/rooms/spaces. So the client can easily identify what items are going where. E.g. Ground floor - open plan office, meeting room 1, meeting room 2, tea point, etc.

#### Flooring and Décor

I would usually put all this information onto the respective drawings. But when needed, I would create a specification spreadsheet the same as above.

### 5. Visualisation

With the joy of using Revit, this step is made easier to start as I will usually have all the basics built up and ready to go in 3D.

Now it’s about getting all those detailed decisions showcased to the client, right? All the correct furniture and fabric choices, wall and floors, lighting etc.

3D visuals can sometimes make or break your design - the difference between a green or red light on an interior design project going ahead.

Visualisation is very labour intensive. I will only create these if the client is showing seriousness to go ahead with the project or has requested them.

The more I worked in this industry the more I realised that some can visualise things easily, and some can’t. What is best for one client may not be for another!

This varies from client to client, project to project. I discern this early on if this stage is needed to get to the end result.

#### Software

I currently use Revit's built-in cloud rendering. But will be expanding this when needed, into a plug-in like Enscape or other apps like Twin Motion. Plenty out there to get stuck into!

### 6. Bespoke and Detail Drawing

Bespoke joinery and detail drawings are pretty standard in interior design projects. This may be cabinetry, glass manifestation, or the construction of bespoke furniture. E.g. media units, wood slat walls, signage, doors etc.

These drawings can be done in step 3 above. But nailed down later,  once final quotations and drawings need to get sent off. To be constructed by relevant suppliers and manufacturers.

I’m a sucker for this drawing type, my brain enjoys solving problems. Seeing the finished result and how small details impact the space and the user is so rewarding!

### 7. Graphic Presentations

Ok, so I have all of this information, but how do I communicate it all to the client? Without them feeling overwhelmed with a million PDFs of each of the above.

I create a design document, with the relevant information needed to give clients the overall project visuals. I only show smaller details here if it is a vital talking point in the brief - and if I think the client needs to see this level of detail at this point.

I will present this design document to the client.

I only use the ‘visuals’ here, so something as follows:

- Intro brief with a mood board
- Furniture plans - I will sometimes have images of the relevant furniture, finishes and décor to talk about on the same page, and an illustrated plan showing colour choices helps
- Some of the design-led flooring and décor plans - illustrated so it shows the specific flooring finish and colour choices
- Sections and elevations related to the above
- 3D visual images created
- Any detailed drawings that are relevant are to be presented at this stage.

I usually have a small amount of text on these pages, so that if for any reason I am not there when they see it, the client can read the information without me!

#### Software

I use a combination of Adobe Illustrator and Adobe InDesign to pull all the needed information into a presentation design document. Using templates I have made previously. I stamp business or company branding on this - for an extra personal touch!

### There you have it.

As I said, this is not an exhaustive list. But a good basis for projects I generally deal with!

Do you have a favourite part of the process? Or a least favourite?

My favourites are the:

- Drawings packs
- Specification and FF&E (I love a good spreadsheet)
- Detail drawings
- Graphic Presentations

## Need a Freelancer Interior Designer?

Let’s chat! I would love to get to know you and your design business. Grab a cuppa, and book a [call with me]({{ site.bookacall }}).

**Do more of what you love and less of what you don’t 😊**