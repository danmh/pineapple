---
layout: blog
category: blogs

# As your title might change when tweaking content, give it a short, recognisable and unique url.
# For example the article '14 reasons why I went freelance' could have the permalink '/goingfreelance/'
# This would then appear at mizziharris.com/goingfreelance .
permalink: /The-hybrid-working-and-RTO-dilemma/

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'summary' section for now
  keywords: :summary

blog:
# This is the title that displays on the page
  title: "A workplace designer's thoughts on hybrid working and the RTO dilemma"
# Author and visible date appear in the format 'Written by {author}, {visible-date}.
  author: "Becca"
  visible-date: "Aug 2023, 6 min read"
# This is your short extract, it will show on shares and search engines, make it good.
  summary: "Read my thoughts as a workplace designer on the office climate in 2023. An opinion and persoanl take on hybrid working, return to office mandates and it's implications."

---

<!-- Add your content using markdown formatting below.                                   -->
<!-- See https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet                -->
<!-- Add images to /assets/images/blog/postname . Reference images using cloudinary.     -->
<!-- Example: https://mizziharris.mo.cloudinary.net/portfolio/blog/examplepost/image.jpg -->

The ‘office’ is still under scrutiny since Covid-19 shook the world back in 2020.

Not to say that the office worked for everyone before then. But the rules of engagement were clearer and unquestioned. We went to the office, and sat at our desks from 9 til 5, with a little break - 5 days a week.

But what about now, in 2023?

We all became accustomed to remote working during Covid-19 lockdowns. To some people’s dismay,  it worked well for the majority of office workers.

The office worker was able to do their bit from home. Yes, we may have been scrambling around. While the right systems were put in place to enable us to do so. (Servers, file access, phone redirects etc). Once restrictions were beginning to lift, people started to mix and socialise again. (Do you remember how weird that was?!)

Some employers wanted workers to come back to the office. This is why hybrid became the happy medium for the employer and employee post-pandemic.

### Hybrid working

As a workplace designer, helping clients create offices that people want to come to. I understand the challenges for employers. Paying leases, rent, utilities etc is uneconomical with an empty office with no staff inside to use it. Companies want the office filled with people at their desks, looking productive. Keeping that office ‘buzz’. The office can be a great place to be, I design many of them after all! But only if there's autonomy, trust, flexibility and a great culture (that starts at the top and works down).

Some workers are happy with the hybrid approach. I mean, it’s more than what most had before 2020? So you can’t complain, right?

But are we happy with hybrid working? Is it effective? And are companies happy with having quiet or empty offices for part of the week?

I am hearing more of companies changing their policies. Trying to enforce staff to have a mandatory 3-4 days in the office. Some are basing this on staff members’ distance from the office, and some are applying a blanket rule for all staff. Regardless of the circumstances or location of each staff member.

On paper, if you’re a full-time worker, going 2 days WFH (working from home) to 3-4 may not seem like a biggy - to most. Especially if you were full-time office working pre-2020.

Before I dive in - For remote and/or hybrid to work successfully, the right systems need to be in place. File access, asynchronous communication, a system for writing down and recording project and company knowledge (so it’s not just stored in specific people’s minds).

I understand hybrid or remote working isn’t for everyone. Whether that be home life setup, personal preferences, location, tech accessibility and the type of role you have.  But I’ll come back around to this later on.

### What enforced ‘office’ days would look like (for me)

As a woman, wife, mother of 2, and in an office-based job, living in South Wales, UK. Here’s my take on it, and I’m going to go into the nitty gritty full routine - for full effect. (If I was employed, in the office 5 days a week, Monday - Friday, 9-5.30 pm)

#### Astronomical childcare costs

Private nurseries cost anywhere between £60-80 per day in my area.

With my children’s current ages, 5 days a week childcare would cost around £1500 (minimum). That’s including government tax-free childcare accounts for each child. And 30 hours free per week for my eldest. *Yes, it is that expensive.*

#### Need to buy a 2nd car

Enabling me to drive to and from work, travelling around 10-15 miles from home to the nearest city. My remote-working husband would do the children’s drop-offs in the 1st car. That’s if I wasn’t able to do any of these because of meetings or heavy traffic etc. Or if we get a call to say they need picking up because they are ill in the middle of the working day. (I couldn’t get there in 15-20 mins as childcare providers would insist).

#### Hardly seeing my children

I’d see them for 30 minutes (if that) before school/nursery in the morning. And for 30 mins for bath and bedtime, if traffic didn’t hold me back. Aka 1 hour a day with my kids on a weekday. *(Time spent wrestling my kids into clothes in the AM, and into bed at PM…)*

#### Quality time with loved ones

If my husband is the one doing all the pick-ups and drops off, he's not able to work his 7-8 hours in ‘normal’ working hours. From September, picking up our eldest from school at 3 pm, and then the younger one from nursery at 5.30 pm, he’ll be working from 9 am -3 pm. Then working in the evening when the kids are in bed to catch up on around 2 hours lost work time, 8-10 pm?

#### Healthy lifestyle

By the time the kids are in bed, we’re cooking dinner at 7.30 - 8 pm. Eating at 8.30 pm? While my husband is trying to catch up on his work still. I would like to fit some exercise a few times a week. I could go for a run once my food has gone down at say, 9 pm. A shower and in bed by 10 pm would be great so I can get my 8 hours of sleep in to do it all again tomorrow. Or I try to sneak out at 6 am (without waking the kids) to get my run in.

#### Life admin, looking after a house… life in general

I struggle to get anything done in the weekday evenings at this rate.

I/we still need to:

Wash, dry, and put away 4 people’s clothes.

Keep on top of cleaning the house.

Filling and emptying the dishwasher.

Tidying toys away.

Food shopping.

Cooking and food prep.

Admins tasks like booking and attending appointments like dentists, vets, doctors, haircuts etc.

Talking to or seeing friends and family.

Do something for me, like a hobby?

### 2-day weekend?

Weekends become a juggling act. Trying to spend quality time with my husband and kids who I have hardly seen all week. All whilst trying to do all the above jobs. Until we do this all again Monday.

I’m not writing all this for a pity party. I am also aware that I am very lucky that I have a partner t home, who is remote working so we could even achieve the above. If he was office based 9-5 too, then what? It would be a toss-up between who earns the most and stays at work, and the other one says bye-bye to their job. We all know how that story usually ends with the gender pay gap…

This is the reality for so many people. My circumstances are 1 set of circumstances. This isn't an exclusive problem for women or parents either. This is to anyone who has caring responsibilities, or to anyone who wants a good work-life balance.

### Work to live, or live to work?

Hybrid-only works if it works for both employer and employee. There is no one size fits all. Which is the sticking point in our current climate. We’re wrestling with each individual’s circumstances. Having rules that work for 1, or even the majority could be catastrophic for another. Catastrophic enough that they aren’t able to hold that job anymore.

(You can read more about my experience on this first-hand [here](https://www.mizziharris.com/my-start-up-story/)).

Companies are struggling to find talented staff and retain staff. I know a lot of people would jump ship for better autonomy, trust and flexibility in their roles. Salary used to be the main factor for people moving on. But that’s becoming less the case as we progress through this RTO (return to office) tug of war. Work-life balance (if you believe in such a thing) is the goal.

### Conclusion

I know what you’re thinking, Becca, it’s clear you favour hybrid or remote working after what you have written. But it’s not the case! There are pros and cons in every person’s case, and there is more work to be done, to make a workplace work, for everyone.

As a freelance interior designer now, working remotely. Believe it or not, I miss the office environment and chatting with colleagues. But I find myself way more productive at WFH without those office distractions. Even whilst being able to put the washing machine on while I make a cuppa!

There’s so much data out there on hybrid and remote working, and companies to look at who are nailing it. managers and companies need to talk to and work with staff on their needs. Which in turn will create loyal, happy and productive staff.

Trust is huge. If staff feel trusted to get the work done, and have autonomy to make decisions. They are way more likely to want to work for their employer, be loyal and care about company outcomes. No one likes feeling like another cog in the wheel. People want to feel like they are making a difference, doing good work and proud to work at said company. Management has a huge say in the company culture and mindset. It all feeds down from the top.

### The Office

Of course, the office building itself can play a big part in staff wanting to commute and enjoy being at the office.

How is the office space?

Are there areas for different kinds of work?

Is it comfortable?

Does it feel like a nice place to be?

There is so much you can do to make office work a bigger hit with staff. And I will be writing a blog on this soon, don’t you worry.

The office can be a productive, inviting and fun place to be and build relationships. Let’s work on that!

### Insightful reads

Here are a amongst a few articles I have read on this topic, if you’re interested to dig in further. It's fascinating how the results and research outcomes are very similar globally.

Worklife.news - RTO Employee Pushback [here](https://www.worklife.news/culture/companies-struggle-rto-employee-pushback/)

Parlament.uk - Research on 'The impact of remote and hybrid working on workers and organisations' [here](ttps://post.parliament.uk/research-briefings/post-pb-0049/#:~:text=Available)

Peoplemanagement.co.uk - Employees feel more productive working from home [here](https://www.peoplemanagement.co.uk/article/1811252/half-employees-feel-productive-when-working-home-research-finds#:~:text=While)

Worklife.news - Leadership productivity measurements [here](https://www.worklife.news/leadership/productivity-measurements/)

## Need a Freelance Workplace Designer?

Let’s chat! I would love to get to know you and your design business. Grab a cuppa, and book a [call with me]({{ site.bookacall }}).

**Do more of what you love and less of what you don’t 😊**

