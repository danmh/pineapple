---
layout: blog
category: blogs

# As your title might change when tweaking content, give it a short, recognisable and unique url.
# For example the article '14 reasons why I went freelance' could have the permalink '/goingfreelance/'
# This would then appear at mizziharris.com/goingfreelance .
permalink: /how-to-work-with-me/

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'summary' section for now
  keywords: :summary

blog:
# This is the title that displays on the page
  title: "5 easy steps to work with me - a freelance interior designer, for businesses"
# Author and visible date appear in the format 'Written by {author}, {visible-date}.
  author: "Becca"
  visible-date: "Feb 2023, 3 min read"
# This is your short extract, it will show on shares and search engines, make it good.
  summary: "Learn how to work with a freelance interior designer to reduce stress and increase productivity in your business. Follow these five simple steps to access freelance interior design services tailored to your needs. Read the blog post to find out more!"

---

<!-- Add your content using markdown formatting below.                                   -->
<!-- See https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet                -->
<!-- Add images to /assets/images/blog/postname . Reference images using cloudinary.     -->
<!-- Example: https://mizziharris.mo.cloudinary.net/portfolio/blog/examplepost/image.jpg -->

I've been speaking with lots of my interior design peers and friends of late,  and I'm hearing a lot of the same things. Stressed with deadlines, not enough time, working late. More interior design support needed and wanted.

It's awesome to hear that business is steady right now in the world of interiors, and having yourself and/or your team at full capacity, working on exciting projects. But from experience, being the stressed interior designer is no fun.

It's hard to feel seen by management; expectation to work evenings here and there to hit deadlines. Which often feels like 'suck it up' until it passes.

More often than not, it doesn't pass, and staff become unhappy and burnout. It's a safe way to get people looking for new opportunities elsewhere.

I've been the burnt-out designer, the sucking-it-up designer, and the not-enough-work designer. I'm here to tell you, there's another way!

Here are 5 easy steps to access my commercial freelance interior design services.
For awesome interior design businesses.
As easily and quickly as possible...

### 1. Lets get to know each other

Book a free coffee chat with me, get to know each other and see if we’re a good fit. You can chat about what problems you need solved and timescales, or tell me about your pets and kids, unload, I don’t mind! I'm an open book, and love getting to the humans behind the business.

It can be what you make it, sometimes you need to know if you’re values align and if someone will gel with you and your team. I'm all for making sure you like who you are about to work with. It's crucial to the beginning of a great working relationship.

And of course, we can talk about software, processes, ways of working. I want to help interior designers and design teams excel, in the best way that works for you. Fill me in!

Software I currently use:
- Revit
- AutoCAD
- Illustrator
- Photoshop
- InDesign
- SketchUp
- Software for spreadsheets, documentation and presentations.

### 2. Create your brief

What tasks do you need off your plate? These can be simple or complex tasks.

- Mood boards
- Drawing packs
- Client presentations
- Revit models, SketchUp models
- Create schedules for FF&E, doors, signage, décor
- Specification documentation
- Detail drawings for joinery, bespoke cabinetry, bespoke fittings

Why heck, you may even want to hand a whole project over and have me deal with the client side too.

Whatever makes your shoulders drop once it’s off yours's or your teams to do list!

### 3. Book my time

Once you know what you need, I will offer the best value for money solution for you.

The rates I offer:
- Full Day
- Half Day
- Hourly
- Per project

All businesses are different. What's best for you, may not work for another business. So I like to offer flexibility here.

You may want me to work X amount of days a week for a longer period.

Or you have sporadic unpredictable work loads, so want an ad hoc hourly rate.

Or, you may have a specific list of things to do and want a price for completion of those tasks. This could be a whole project, or one set of tasks. A project price, made for you.

I'm open to new ways of working. Feel free to suggest a work set up that will work best for you and your team, and I will accommodate what I can!

My freelance interior design services are remote. But if you are in Cardiff, Vale of Glamorgan and South Wales regions, I may be able to get to you in person too. I'm based in Barry, South Wales.

### 4. Get work done

If you have a deadline, I will meet it and give you my timescales to complete.

I will always keep you updated, via call, email, messaging app of choice - whatever medium works best for you. We will work together to get the work done!

A clear brief is always best, and my personality and work style is thorough and detail orientated.

So, I ask questions where needed. To make sure I'm getting it right first time, and insuring I am up to speed on your processes.

If there's anything I have learnt in my interior design career so far... Don't make assumptions, don't be afraid to ask questions, avoid costly mistakes.

### 5. Invoice and repeat

I invoice every 2 weeks on any works completed, days, hours worked. You can keep coming back for more whenever you need it.

**If you're ever in crisis, please reach out as I may have availability to help you last minute. I mean it when I say I would love to help!**

## Want to get started and find out more?

Lets chat! Grab a cuppa, and book a [call with me]({{ site.bookacall }}).