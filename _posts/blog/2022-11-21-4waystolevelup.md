---
layout: blog
category: blogs
permalink: /4-ways-to-grow/

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'summary' section for now
  keywords: :summary

blog:
  title: "4 ways to level up your interior design business"
  author: "Becca"
  visible-date: "Nov 2022, 3 min read"
  summary: "Learn how to level up your interior design business with the help of a freelance interior designer. This blog post offers four easy ways to increase your capacity and output, grow your business, add to your skill set, and learn from others. Whether you're a small or solo business owner, hiring a freelancer can help you take on more work, increase revenue, and optimize your best practices. If this sounds like you, give this blog post a read."

---

<!-- Add your content using markdown formatting below.                                   -->
<!-- See https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet                -->
<!-- Add images to /assets/images/blog/postname . Reference images using cloudinary.     -->
<!-- Example: https://mizziharris.mo.cloudinary.net/portfolio/blog/examplepost/image.jpg -->

Before I get into it, I want to give you a little context!

I’ve been the only interior designer in a commercial interior design business. Having a few projects on the go, and then tendering for £1m+ projects. It's a lot to take on.

I was the go to for interior design, furniture specification, liaising with suppliers. Pulling all pricing, design and final information together to present to the client. Spreadsheets, design and presentation documentation, the whole thing.

There was too much to do in my 9-5. Squeezing work into my evenings (after nursery runs and bedtime routines). Burn out building, and stress became all consuming as my personal time got stripped away. 

I remember asking management to hire another designer, because I couldn’t go on as I was. (Not the first time I had asked this in my career!)

Responses were always “if it’s this busy for months, we will consider hiring, but not right now”.

## Hiring staff

Hiring a staff member can be risky for businesses. The expense of permanent salaries, HR, annual leave, equipment, licenses, and facilities. Not forgetting of course, enough work to keep everyone in a job. No one wants to go through a hiring process, to then have to let someone go after what was only a temporary busy period!

But, I was burnt out. Having someone in, one day a week to complete time consuming tasks would have made the world of difference to me.

Which in part, has lead me to where I am today. A freelance interior designer, for interior design businesses.

Here’s 4 easy ways to get help and grow your interior design and architecture businesses right now...

### 1. Increase your capacity and output

Running a business is tough. Especially if you are a small or solo business owner. Lots of hats you need to wear everyday. Marketing, finance, admin, prospecting, upskilling and networking. Finally, the job you actually set out to do in the first place, Interior Design (in this context).

There is never enough time in the day to do everything, right? So, lessen the load, get the time zapping tasks off your plate so you can focus on the stuff you NEED or WANT to do. Whether it be client presentations, survey drawings, 3D models, scheduling or mood boards. You name it, lets get it off your plate.

### 2. Grow your interior design business quicker

How do you grow when you are already at full capacity? You need time to win more work, and grow your client base, fill that pipeline.

Good news is, you can hire and drop a freelancer based on your business needs. No hard conversations needed. Take what you need, and leave the rest. Any sized projects and timescales, and quick start and turnaround times.

Hire a freelancer, will give you time to take on more work. You will be able to grow your business and increase revenue. Giving you capacity to hire a permanent staff when that pipeline is replenishing.

### 3. Add to your business skillset

Clients expecting and asking for expertise that you don’t feel confident delivering? Bring someone in!

It can be software specific drawings and models, visualisations, technical drawings or scheduling.

Whatever the task, a freelancer can add skills to your team without you having to spend hours learning how. Or having to turn down a project because of 5% of the deliverables - when you can deliver 95%.

### 4. Learn from others

If you are a solo designer or part of a small team, it can be hard to learn new things when you don’t have people to work alongside in a similar role. Especially when there’s no time or capacity in the day to stop or do some training.

Working with a freelancer is a great way to expose yourself or your team to someone with different skillsets and ways of working. It's awesome to see how all our design processes differ! I love seeing behind the scenes and how people tackle different design elements. It’s a great way of improving and optimising your best practices. Always room for learning and growing.

*(Something I miss about being part of a wider team - working through design problems with others!)*


## Want to grow your business?

Lets chat! I would love to get to know you and your design business. Grab a cuppa, and book a [call with me]({{ site.bookacall }}).
