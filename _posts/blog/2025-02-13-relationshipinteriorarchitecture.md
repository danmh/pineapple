---
layout: blog
category: blogs
permalink: /interior-architecture-relationship/

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'summary' section for now
  keywords: :summary

blog:
  title: "4 reasons why Interior Design and Architecture is a relationship to invest in"
  author: "Becca"
  visible-date: "Feb 2025, 4 min read"
  summary: "Learn more about how the relationship between interior design and architecture intertwines, and why including interior design in the process from the outset is key to successful spaces and happy people."

---

<!-- Add your content using markdown formatting below.                                   -->
<!-- See https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet                -->
<!-- Add images to /assets/images/blog/postname . Reference images using cloudinary.     -->
<!-- Example: https://mizziharris.mo.cloudinary.net/portfolio/blog/examplepost/image.jpg -->

Seeing as it’s valentine’s day, it’s the perfect opportunity to reflect on a recent chat about relationships that I had not too long ago on the podcast. However it wouldn’t be very [Design and Build](https://open.spotify.com/show/5ePM0XDJva9WIR4qjlZ5Zv?si=c21e222e72d34b73), if the theme was romance. So instead, lets talk about where architecture meets interior design, and why we should invest more time in that relationship.

Joined by Bristol based interior designer and architectural photographer [Kenton Simons](https://www.linkedin.com/in/kenton-simons/) from [Story Design](https://www.story-photography.co.uk/), naturally the conversation turned towards the intersection of his two roles, being in the Interior Design and Architecture industry for 25+ years. His unique insight had me thinking about the often overlooked importance of design partnerships. Drawing from some of the valuable points Kenton shared with me during recording, here’s four reasons why including an interior designer early in the architectural design process is the key to creating a successful space.

### 1. Avoiding conflict

Kenton described the challenges of finding a balance, where architecture stops, and interiors begin. Often, there’s an overlap that can create tension, but it’s in this overlap that you’ll also find the potential to develop a seamless project from start to finish. In this relationship, timing really matters, and the balance needs to be right. Kenton emphasized how vital it is to involve interior designers early in a project. If interiors are treated as an afterthought, the final result is often disjointed. However, when both disciplines work hand-in-hand from the start, they can anticipate challenges and create solutions that feel intentional. Giving the space and building a truly harmonious outcome. Where they gel and intersect perfectly. *Chefs kiss*

### 2. Maximise impact

Like any great partnership, it’s about understanding strengths of each role when bringing a space to life. Architecture provides the foundation, the structure, while interior design shapes how people experience and use the space. Day in, day out. 

We both have experienced the difficulty of interior design being undervalued in the process, whether that be by contractors, architects, builders. More respect and attention is needed to the fact that interior design is about way more than just ‘picking the colours’. Both roles are about functionality, efficiency, and creating spaces that truly work for their intended purpose. When you allow them to coexist is when you unlock the ability to really maximise impact and create buildings and spaces that people want and love to be in. 

Every project is unique, and just like how Kenton shared how he tailors his work to reflect a client’s brand or personal story - pairing structural design and interior elements together are the optimal way to tell a cohesive narrative.

### 3. Save time & money

Early involvement of interior designers isn't just about better design - it's about cost efficiency. When interior designers join a project late, we often find ourselves trying to retrofit solutions into a space that wasn't planned with the end use in mind. This leads to compromises, additional costs and missed deadlines.

We discussed some of the common late-stage changes on the podcast, which can include modifying ceiling layouts for lighting features and adding power and data points after walls are built. In extreme cases, you can even end up undertaking full structural modifications to fulfil space planning requirements. We need to avoid this where possible, it's a drain on resources, sustainability goals and the clients budget.

### 4. Foster creative collaboration

One of the key takeaways from our podcast was Kenton’s approach to collaboration. He talked about fostering ‘collaborative relationships’ with not only clients but also the teams working on the project. This mindset is the crucial ingredient that fosters an open dialogue between designers that allows them to honour the client’s vision with ease.

As we reflected on past experience, a common theme became clear - that sharing ideas and working together without a doubt creates a more impressive end result. Interior design isn’t just about filling a space with furniture; it’s about understanding how we can utilise architecture to enhance the way people live, work, or play. And when we’re given a say in that architectural design process, we can really achieve the best result for the client and the users of the space.

It all starts and ends with good relationships, communication and collaboration.

## Want to hear more?

In Episode 9 of [Design and Build](https://open.spotify.com/show/5ePM0XDJva9WIR4qjlZ5Zv) podcast, we explore the intersection of architecture and interior design, the role of client relationships, sustainability in design, the tools that grow and develop with us designers, and how Kenton’s dual role as an interior designer and architectural photographer gives him a unique perspective when he's got his photography hat on!

You can listen to the full podcast episode [here](https://open.spotify.com/episode/42SGMcGYPvyibIm7zw3Tw7?si=f3601dc50fb84a23). Enjoy!

## Looking for help and collaboration in your interior design business?

Find out more about my freelance interior design services. Grab a cuppa, and book a [call with me]({{ site.bookacall }}). I would love to hear from you 😊