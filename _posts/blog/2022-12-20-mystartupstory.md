---
layout: blog
category: blogs
permalink: /my-start-up-story/

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'summary' section for now
  keywords: :summary

blog:
  title: "A start up story - why I chose freelance interior design"
  author: "Becca"
  visible-date: "Dec 2022, 4 min read"
  summary: "Why I started my freelance interior design business to help interior design businesses, This blog post outlines the reasons why I started my freelance interior design business. Discussing my career goals, problem-solving abilities, flexibility, and job satisfaction. Talking about my experience in branding and creating inspiring spaces for commercial interiors. If you're interested in learning more, give this blog post a read."

---

<!-- Add your content using markdown formatting below.                                   -->
<!-- See https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet                -->
<!-- Add images to /assets/images/blog/postname . Reference images using cloudinary.     -->
<!-- Example: https://mizziharris.mo.cloudinary.net/portfolio/blog/examplepost/image.jpg -->


Don't you love the story behind a business, and what makes people tick? I know I do! So here are 5 from me at Stiwdio Mizzi-Harris.

### 1. Career Goals

While in Sixth form,  I got myself a retail job at New Look on the weekends. I was able to work this job while studying for my BA Interior Architecture degree at Cardiff School of Art & Design, bouncing between different stores. Over the six years spent there, I worked my way up through increasing hours, responsibility and visual merchandising, and finally into a management role.

I loved working with people, the fast pace, the team effort, and even the visual merchandising night shifts. That deadline rush to get the shop looking good and ready for opening that morning was a such buzz! I have always had the drive to push myself and to be able to do this as a team (even in the middle of the night!)

Working for myself was a daunting thought back then though, and I was a complete newbie in Interior Design. I wanted to learn so much more from other designers in the field first. I had my degree, and experience in Visual Merchandising (watching in real time how people interact with space and the product you have strategically placed was great!) 

My Interior Architecture degree taught me so much about space, architecture, and building heritage. It went way beyond the nice and fluffy stuff like cushions, sofas and styling. We never learned anything about furniture and styling actually -that's the stuff I learnt on the job with other interior designers.

After seven years of working in Commercial Interior Design, I soaked up as much knowledge as possible, took the plunge, and set up Stiwdio Mizzi-Harris in 2022.

### 2. Problem-solving

As a child, I reconfigured my bedroom continuously (to my mother's dismay). There was likely a goal in mind. I didn’t like the way my bed was facing, I fancied a change, or I had something new I wanted to accommodate.

This problem-solving attitude was nurtured and encouraged while working in retail and visual merchandising. If someone told me they couldn't fit stock into a particular space, I would always find a solution.

*Also, I am a millennial. And, as a friend pointed out to me recently, I spent most of my childhood playing The Sims. Designing and building houses, planning spaces, and decorating and furnishing them. It has something to do with my career choice whether I like it or not!*

How does this translate into my business today?

I want to help solve problems for others. It fuels me when it comes to my Freelance Interior Design offering.

Whether it be ‘how many people can we seat’ or ‘I’ve only got X amount to spend on this refurb’. Or, more complex problems like burnout, tenders, deadlines, and needing more designers.

I want to help interior designers and businesses get the help and resources they need when they need it. Having a Freelance Interior Designer on your books will bring a sigh of relief. When you need to get through intense workloads and deadlines.

### 3. Right projects, right clients

One major benefit of going solo is being able to choose what clients you want to work with and the kind of projects you want to take on.

As I mentioned, I love working with people - and especially like-minded people. If we are a good fit and gel well, perfect!

My experience over the last seven years has been in commercial interior design. I worked on a full range of small to large office and workplace design schemes - from brief to completion. Also, designing café spaces, healthcare facilities, and residential properties.

I am passionate about creating inspiring places for people to experience. In office design, helping shift the mentality of offices being desks and boring, 9 to 5 places to escape.

It also ties into another passion of mine, branding.

*Little side fact: I used to freelance graphic design for a few years self-employed (alongside my day job) and run an Etsy shop selling greeting cards and prints. My experience with residential interior design happened during my self-employment too!*

I enjoy adding brand personality to a space. It can bring a brand to life when it can live and breathe in physical spaces! It doesn’t have to feel corporate. Or as if you’re not letting people escape ‘the brand’. But working with your values and the people of your company is so important in office design. As a Freelance Interior Designer, these are the problems I thrive on solving.

### 4. Flexibility

This may be an obvious one, but it’s important to me to be transparent and accessible, as an individual and a woman.

I started Stiwdio Mizzi-Harris in March/April 2022. I was coming off the back of my second maternity leave. After a weird couple of years of Covid-19 lockdowns and bouts of furlough.

The commercial interior design industry was in a weird place. At least within the realms, I was working in South Wales. I knew I didn’t want to return to my employment, I needed a change of pace and a new challenge!

Continuing with the real talk, I am privileged. I have a husband who can cover most of the bills, and it felt like a great time to set up a business. With lower risk and without the added pressure of having to earn ££££ a month to keep a roof over my family’s heads. Don’t get me wrong, it still was a scary move for me, and I put plenty of pressure on myself to achieve my goals. (A blog for another time?)

I work a consistent 3 days a week, Tuesday - Thursday. But if I need to move things around, I have the flexibility to do that as a Freelance Interior Designer.

It also means if I get burnout and need a break, book a dentist appointment, or the kids get ill. Between me and my remote worker husband, I can make it work!

### 5. Job satisfaction

And last but not least. I have loved working in Commercial Interior Design over the last seven years. But, I love it more now as a Freelance Interior Designer.

I appreciate the little things. The client calls, people, architecture, building history, and company cultures. Working with materials, flooring, fabrics, furniture, textures, and layouts. Bringing an idea to life in a physical space to live and worked in.

As an Interior Architect, I dig into what a space is and means to people. I get my creative kick by pulling an interior design scheme together. But I value and appreciate the results much more. Happy clients :)

More problems solved and fewer headaches for fellow designers and businesses. I love my job! 

## Need help to achieve goals and hit deadlines?

Lets chat! I would love to get to know you and your design business. Grab a cuppa, and book a [call with me]({{ site.bookacall }}).
