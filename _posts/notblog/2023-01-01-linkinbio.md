---
layout: blog
category: notblog

# As your title might change when tweaking content, give it a short, recognisable and unique url.
# For example the article '14 reasons why I went freelance' could have the permalink '/goingfreelance/'
# This would then appear at mizziharris.com/goingfreelance .
permalink: /findme/

meta:
# I think keywords are for SEO, I've set them to just copy what is in the 'summary' section for now
  keywords: :summary

blog:
# This is the title that displays on the page
  title: "Find me online 👋"
# Author and visible date appear in the format 'Written by {author}, {visible-date}.
  author: ""
  visible-date: ""
# This is your short extract, it will show on shares and search engines, make it good.
  summary: "Rebecca Mizzi-Harris, find me all over the web."

---

<!-- Add your content using markdown formatting below.                                   -->
<!-- See https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet                -->
<!-- Add images to /assets/images/blog/postname . Reference images using cloudinary.     -->
<!-- Example: https://mizziharris.mo.cloudinary.net/portfolio/blog/examplepost/image.jpg -->

<style>.blog-byline { display: none; }</style>

Here's everything you need:

Connect with me on [LinkedIn]({{ site.linkedin }})

Give me a follow on [Instagram]({{ site.instagram }})

Find everything on my [website, including blog and portfolio](/)

Have a listen to the [Design &amp; Build podcast]({{ site.podcast }})

Find out more and [book a call with me]({{ site.bookacall }})

Send me a message via [Email](mailto:{{ site.email }})

Join my mailing list [Stiwdio Stories]({{ site.newsletter }})