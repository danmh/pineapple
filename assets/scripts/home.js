var sr = ScrollReveal({
	origin   : "bottom",
	distance : "64px",
	duration : 800,
	delay    : 0,
	scale    : 1
});

sr.reveal('.projects-list a');
sr.reveal('.posts-list a');

function toggleLatestPodEpDesc() {
	var content = document.getElementById("latest-pod-ep-desc");
	var link = document.getElementById("toggle-latest-pod-ep-desc");

	if (content.classList.contains("line-clamp")) {
		content.classList.remove("line-clamp");
		link.textContent = "Show Less";
	} else {
		content.classList.add("line-clamp");
		link.textContent = "Show More";
	}
}

// Check if content is clamped
function isTextClamped(element) {
	return element.scrollHeight > element.clientHeight;
}

// Hide "Show More" link if content isn't clamped
window.addEventListener('load', function () {
	var content = document.getElementById("content");
	var link = document.getElementById("toggle-content");
	if (!isTextClamped(content)) {
		link.style.display = 'none';
	}
});