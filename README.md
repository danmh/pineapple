# Portfolio

This website is forked from the Pineapple Jekyll theme by [arnolds](https://github.com/arnolds/pineapple).

## GitLab pages

Site hosted and deployed through GitLab pages.

## Cloudinary

Images are optimised and served using Cloudinary Media Optimizer.

### Base transformation

```
c_limit,h_2000,w_2000,f_auto,q_auto:good,
```

### Media source

```
https://portfolio.mizziharris.com/assets/images
```

## Setup

Install dependencies:

```
$ gem install jekyll bundler
```

Pulldown the project:

```
$ git clone git@gitlab.com:danmh/pineapple.git
$ cd pineapple
```

Start Jekyll:

```
$ jekyll serve -w -l
```

Browse to http://127.0.0.1:4000/pineapple/ for some Pineapple goodness.

## Creating projects

Projects are created as `.md` documents within the `_posts/projects` directory. They follow the same naming conventions as regular [Jekyll posts](https://jekyllrb.com/docs/posts/).

## License

Code open sourced under the [MIT license](LICENSE.md).

Content ⓒ Rebecca Mizzi-Harris